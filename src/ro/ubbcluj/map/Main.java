package ro.ubbcluj.map;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.RepositoryFactory;
import ro.ubbcluj.map.repository.StrategyPersistency;
import ro.ubbcluj.map.repository.StrategyRepository;
import ro.ubbcluj.map.service.FriendsipService;
import ro.ubbcluj.map.service.UserNetworkService;
import ro.ubbcluj.map.service.UserService;
import ro.ubbcluj.map.tests.Tests;
import ro.ubbcluj.map.ui.UI;

import javax.swing.text.Utilities;
import java.util.Objects;


public class Main {
    public static void main(String[] args) {
        Tests tests = new Tests();
        tests.run_all_tests();

        Repository < Long, User > repoUser = RepositoryFactory.getInstance().createRepository(StrategyPersistency.valueOf(args[0]), StrategyRepository.USER);
        Repository <Pair< Long >, Friendship > repoFriendship = RepositoryFactory.getInstance().createRepository(StrategyPersistency.valueOf(args[0]),StrategyRepository.FRIENDSHIP);
        UserService userService = new UserService(repoUser);
        FriendsipService friendsipService = new FriendsipService(repoUser,repoFriendship);
        UserNetworkService userNetworkService = new UserNetworkService(repoUser);
        UI ui = new UI(userService , friendsipService , userNetworkService);
        ui.run();
    }
}
