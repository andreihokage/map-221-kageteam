package ro.ubbcluj.map.domain.Exceptions;

public class ErrorAddFriendship extends RuntimeException{

    public ErrorAddFriendship(String msg){
        super(msg);
    }
}
