package ro.ubbcluj.map.domain.Exceptions;

public class ErrorAddUser extends RuntimeException{

    public ErrorAddUser(String msg){
        super(msg);
    }
}
