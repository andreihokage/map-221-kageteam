package ro.ubbcluj.map.domain.Exceptions;

public class ErrorDeleteUser extends RuntimeException{

    public ErrorDeleteUser(String msg){
        super(msg);
    }
}
