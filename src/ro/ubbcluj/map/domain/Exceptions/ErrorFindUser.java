package ro.ubbcluj.map.domain.Exceptions;

public class ErrorFindUser extends RuntimeException{

    public ErrorFindUser(String msg){
        super(msg);
    }

}
