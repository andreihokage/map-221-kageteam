package ro.ubbcluj.map.domain.Exceptions;

public class ErrorRemoveFriendship extends RuntimeException{

    public ErrorRemoveFriendship(String msg){
        super(msg);
    }
}
