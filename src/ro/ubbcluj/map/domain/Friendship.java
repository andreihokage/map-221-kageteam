package ro.ubbcluj.map.domain;

import ro.ubbcluj.map.utils.Constants;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;


public class Friendship extends Entity<  Pair<Long>  >{

    private static Long id_obj = 1L;

    private Long idUserS;
    private Long idUserD;
    private LocalDate date;

    public Friendship(Long idUserS, Long idUserD , LocalDate date) {
        this.idUserS = idUserS;
        this.idUserD = idUserD;
        this.date = date;

        setId( new Pair<Long>( idUserS, idUserD ) );

    }

    /**
     *
     * @return id of the first user from friendship
     */
    public Long getIdUserS() {
        return idUserS;
    }

    /**
     *
     * @param idUserS - set id of the first user from friendship
     */
    public void setIdUserS(Long idUserS) {
        this.idUserS = idUserS;
    }

    /**
     *
     * @return id of the second user from friendship
     */
    public Long getIdUserD() {
        return idUserD;
    }

    /**
     *
     * @param idUserD - set id of the second user from friendship
     */
    public void setIdUserD(Long idUserD) {
        this.idUserD = idUserD;
    }

    /**
     *
     * @return time when the two users became friends
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     *
     * @param date - set time when the two users became friends
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "idUserS=" + idUserS +
                ", idUserD=" + idUserD +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return ( Objects.equals(idUserS, that.idUserS) && Objects.equals(idUserD, that.idUserD) ) || ( Objects.equals(idUserS, that.idUserD) && Objects.equals(idUserD, that.idUserS) );
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUserS, idUserD);
    }
}
