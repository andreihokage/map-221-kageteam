package ro.ubbcluj.map.domain;

import java.util.Objects;

public class Pair <E extends Comparable<? super E>> {
    private E x;
    private E y;

    public Pair(E x, E y) {

        if( x.compareTo(y) > 0 ){
            E aux = x;
            x = y;
            y = aux;
        }

        this.x = x;
        this.y = y;
    }

    public E getX() {
        return x;
    }

    public E getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?> pair = (Pair<?>) o;
        return ( Objects.equals(x, pair.x) && Objects.equals(y, pair.y) ) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
