package ro.ubbcluj.map.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class User extends Entity<Long>{

    //public static Long id_obj = 1L;

    private String firstName;
    private String lastName;
    private List< User > friends;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        friends = new ArrayList<User>();
        //setId(id_obj++);
    }

    /**
     *
     * @return the firstname of the user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName - set the firstname of the user with that string
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return the last name of the user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName - set the last name of the user woth that string
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return user's friend list
     */
    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    /**
     *
     * @param user - add user into friend list
     */
    public void addFriend(User user){
        friends.add(user);
    }

    /**
     *
     * @param user - remove user from firend list
     */
    public void removeFriend(User user){
        friends.remove(user);
    }

    @Override
    public String toString() {
        return "User{" +
                "id= '" + getId().toString() + '\'' +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(friends, user.friends);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, friends);
    }
}
