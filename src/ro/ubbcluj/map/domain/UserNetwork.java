package ro.ubbcluj.map.domain;

import java.util.List;

public class UserNetwork extends Entity<Long> {

    private static Long id_obj = 1L;
    private List< User > listUsers;

    public UserNetwork(List<User> listUsers) {
        this.listUsers = listUsers;
        setId(id_obj++);
    }

    /**
     *
     * @return the list of users from social network
     */
    public List< User > getListUsers(){
        return listUsers;
    }

    /**
     *
     * @return number of users from social network
     */
    public int numberOfUsers(){
        return listUsers.size();
    }

    @Override
    public String toString() {
        String line = "UserNetwork{ ";
        for(User user : listUsers)
            line = line + user + '\n';
        line = line + '}';
        return line;
    }
}
