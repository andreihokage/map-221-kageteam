package ro.ubbcluj.map.domain.validators;

import ro.ubbcluj.map.domain.Friendship;

public class FriendshipDbValidator implements Validator<Friendship> {
    @Override
    public void validate(Friendship entity) throws ValidationException {
        String msg = "";

        if( entity.getIdUserS().equals( entity.getIdUserD() ) )
            msg += "A user cannot be friend of himself!";
        if( entity.getIdUserS() <= 0L )
            msg += "User1 invalid!";
        if( entity.getIdUserD() <= 0L )
            msg += "User2 invalid!";
        if( !msg.equals("") )
            throw new FriendshipException(msg);
    }
}
