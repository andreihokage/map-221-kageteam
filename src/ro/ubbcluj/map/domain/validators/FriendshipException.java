package ro.ubbcluj.map.domain.validators;

import ro.ubbcluj.map.domain.validators.ValidationException;

public class FriendshipException extends ValidationException {
    public FriendshipException(String msg){
        super(msg);
    }
}
