package ro.ubbcluj.map.domain.validators;

import ro.ubbcluj.map.domain.User;

public class UserDbValidator implements Validator <User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String msg = "";
        if( entity.getFirstName() == null || entity.getFirstName().equals("") )
            msg += "First name is empty!";
        if( entity.getLastName() == null || entity.getLastName().equals("") )
            msg += "Last name is empty!";
        if( !msg.equals("") )
            throw new UserException(msg);
    }
}
