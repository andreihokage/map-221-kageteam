package ro.ubbcluj.map.domain.validators;

import ro.ubbcluj.map.domain.validators.ValidationException;

public class UserException extends ValidationException {
    public UserException(String msg) {
        super(msg);
    }
}
