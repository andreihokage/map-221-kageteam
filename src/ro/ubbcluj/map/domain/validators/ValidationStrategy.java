package ro.ubbcluj.map.domain.validators;

public enum ValidationStrategy {
    USERVALIDATION,
    FRIENDSHIPVALIDATION
}
