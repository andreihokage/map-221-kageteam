package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.domain.validators.FriendshipDbValidator;
import ro.ubbcluj.map.domain.validators.FriendshipValidator;
import ro.ubbcluj.map.domain.validators.UserDbValidator;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
import ro.ubbcluj.map.repository.db.UserDbRepository;
import ro.ubbcluj.map.repository.file.FriendshipFile;
import ro.ubbcluj.map.repository.file.UserFile;
import ro.ubbcluj.map.repository.memory.FriendshipInMemoryRepository;
import ro.ubbcluj.map.repository.memory.UserInMemoryRepository;
import ro.ubbcluj.map.utils.DbConnection;

public class RepositoryFactory {

    private static RepositoryFactory instance = null;

    private RepositoryFactory(){

    }

    /**
     *
     * @return an instance of RepositoryFactory
     */
    public static RepositoryFactory getInstance(){
        if( instance == null ){
            instance = new RepositoryFactory();
        }
        return instance;
    }

    /**
     *
     * @param persistence - type of persistency(file|memory)
     * @param strRepo - type of repository(users|friendships)
     * @return specific repository according to persistence and strRepo
     */
    public Repository createRepository(StrategyPersistency persistence,StrategyRepository strRepo){
        if( persistence == StrategyPersistency.FILE ){
            if( strRepo == StrategyRepository.USER )
                return new UserFile("data/user.in",new UserValidator());
            return new FriendshipFile("data/friendship.in",new FriendshipValidator());
        }
        else if( persistence == StrategyPersistency.MEMORY ){
            if( strRepo == StrategyRepository.USER )
                return new UserInMemoryRepository(new UserValidator());
            return new FriendshipInMemoryRepository(new FriendshipValidator());
        }
        else{
            if( strRepo == StrategyRepository.USER )
                return new UserDbRepository(DbConnection.URL,DbConnection.USERNAME,DbConnection.PASSWORD,new UserDbValidator());
            return new FriendshipDbRepository(DbConnection.URL,DbConnection.USERNAME,DbConnection.PASSWORD,new FriendshipDbValidator());

        }
    }

    /**
     *
     * @param persistence - type of persistency(file|memory)
     * @param strRepo - type of repository(users|friendships)
     * @return specific repository for testing according to persistence and strRepo
     */
    public Repository createRepositoryTesting(StrategyPersistency persistence,StrategyRepository strRepo){
        if( persistence == StrategyPersistency.FILE ){
            if( strRepo == StrategyRepository.USER )
                return new UserFile("datatest/userfiletest.in",new UserValidator());
            return new FriendshipFile("datatest/friendshiptest.in",new FriendshipValidator());
        }
        else if( persistence == StrategyPersistency.MEMORY ){
            if( strRepo == StrategyRepository.USER )
                return new UserInMemoryRepository(new UserValidator());
            return new FriendshipInMemoryRepository(new FriendshipValidator());
        }
        else{
            if( strRepo == StrategyRepository.USER )
                return new UserDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new UserDbValidator());
            return new FriendshipDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new FriendshipDbValidator());

        }
    }

}
