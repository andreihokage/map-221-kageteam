package ro.ubbcluj.map.repository;

public enum StrategyPersistency {
    MEMORY,
    FILE,
    DATABASE
}
