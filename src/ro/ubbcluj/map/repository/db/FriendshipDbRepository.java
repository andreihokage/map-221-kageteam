package ro.ubbcluj.map.repository.db;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.Validator;
import ro.ubbcluj.map.repository.Repository;

import java.net.ConnectException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

public class FriendshipDbRepository implements Repository< Pair< Long > ,Friendship > {

    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;

    public FriendshipDbRepository(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Friendship findOne(Pair<Long> id) {
        Friendship friendship = null;
        if( id == null )
            throw new IllegalArgumentException("Id must be not null!");

        String sql = " select * from friendships  where id_user1 = ? and id_user2 = ? ";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql ); ) {

            statement.setLong(1,id.getX());
            statement.setLong(2,id.getY());
            ResultSet resultSet = statement.executeQuery();
            if( resultSet.next() == false )
                return null;

            Date date = resultSet.getDate("friendship_date");
            String pattern = "MM/dd/yyyy";
            DateFormat df = new SimpleDateFormat(pattern);
            String strDate = df.format(date);
            DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            LocalDate dateTime = LocalDate.parse(strDate, DATEFORMATTER);

            Long idUserS = resultSet.getLong("id_user1");
            Long idUserD = resultSet.getLong("id_user2");
            friendship = new Friendship(idUserS,idUserD,dateTime);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return friendship;
    }

    @Override
    public Iterable<Friendship> findAll() {
        HashMap< Pair < Long > , Friendship > friendships = new HashMap<Pair<Long>,Friendship>();

        String sql = " select * from friendships ";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql );
             ResultSet resultSet = statement.executeQuery() ) {

            while(resultSet.next()){
                Long idUserS = resultSet.getLong("id_user1");
                Long idUserD = resultSet.getLong("id_user2");
                Date date = resultSet.getDate("friendship_date");


                String pattern = "MM/dd/yyyy";
                DateFormat df = new SimpleDateFormat(pattern);
                String strDate = df.format(date);
                DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");
                LocalDate dateTime = LocalDate.parse(strDate, DATEFORMATTER);

                Friendship friendship = new Friendship(idUserS,idUserD,dateTime);
                friendships.put(friendship.getId(),friendship);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return friendships.values();
    }

    @Override
    public Friendship save(Friendship entity) {
        if( entity == null )
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);

        Friendship find_friendship = findOne(entity.getId());
        if( find_friendship != null ) //the entity already exist
            return entity;

        String sql = "insert into friendships(id_user1,id_user2,friendship_date) values (?,?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql ) ) {

            statement.setLong(1,entity.getId().getX());
            statement.setLong(2,entity.getId().getY());

            LocalDate dateTime = entity.getDate();
            Date sqlDate = Date.valueOf(dateTime);
            statement.setDate(3,sqlDate);

            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    @Override
    public Friendship delete(Pair<Long> id) {
        if( id == null )
            throw new IllegalArgumentException("Id must be not null!");

        Friendship friendship = findOne(id);
        if( friendship == null )
            return null;

        String sql = " delete from friendships where id_user1 = ? and id_user2 = ? ";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql ) ) {

            statement.setLong(1,id.getX());
            statement.setLong(2,id.getY());
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return friendship;
    }

    @Override
    public Friendship update(Friendship entity) {
        if( entity == null )
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);

        Friendship find_friendship = findOne(entity.getId());
        if( find_friendship == null )
            return entity;

        String sql = " update friendships set friendship_date = ? where id_user1 = ? and id_user2 = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql ) ) {


            LocalDate dateTime = entity.getDate();
            Date sqlDate = Date.valueOf(dateTime);
            statement.setDate(1,sqlDate);
            statement.setLong(2,entity.getId().getX());
            statement.setLong(3,entity.getId().getY());
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }


}
