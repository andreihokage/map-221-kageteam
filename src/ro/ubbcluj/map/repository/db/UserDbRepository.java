package ro.ubbcluj.map.repository.db;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.Validator;
import ro.ubbcluj.map.repository.Repository;

import java.net.ConnectException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserDbRepository implements Repository<Long, User> {

    private String url;
    private String username;
    private String password;
    private Validator<User> validator;

    public UserDbRepository(String url, String username, String password, Validator<User> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    private List< User > getListFriends(Connection connection,Long id){
        List < User > ans = new ArrayList<>();
        String sql = " select id_user1,id_user2 from friendships where id_user1 = ? or id_user2 = ? ";
        try (PreparedStatement statement = connection.prepareStatement(sql) ) {

            statement.setLong(1,id);
            statement.setLong(2,id);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                Long id_user1 = resultSet.getLong("id_user1");
                Long id_user2 = resultSet.getLong("id_user2");
                Long id_search = id_user1;
                if( id_user1.equals(id) )
                    id_search = id_user2;

                PreparedStatement statement1 = connection.prepareStatement("select * from users where id = ?");
                statement1.setLong(1,id_search);
                ResultSet resultSet1 = statement1.executeQuery();
                resultSet1.next();
                Long userId = resultSet1.getLong("id");
                String firstName = resultSet1.getString("first_name");
                String lastName = resultSet1.getString("last_name");
                User user = new User(firstName,lastName);
                user.setId(userId);
                ans.add(user);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return ans;
    }

    @Override
    public User findOne(Long id) {
        User user = null;
        if( id == null )
            throw new IllegalArgumentException("Id must be not null!");
        String sql = " select * from users where id = ? ";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql ) ) {

            statement.setLong(1,id);
            ResultSet resultSet = statement.executeQuery();
            if( resultSet.next() == false )
                return null;
            Long userId = resultSet.getLong("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            user = new User(firstName,lastName);
            user.setId(userId);

            if( user.getFriends().size() != 0 )
                user.setFriends(new ArrayList<>());

            List < User > user_friend = getListFriends(connection,userId);
            user.setFriends(user_friend);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return  user;
    }

    @Override
    public Iterable<User> findAll() {
        HashMap<Long,User> users = new HashMap<Long,User>();
        String sql = "select * from users";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql );
             ResultSet resultSet = statement.executeQuery() ) {

            while(resultSet.next()){
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");

                User user = new User(firstName,lastName);
                user.setId(id);
                if( user.getFriends().size() != 0 )
                    user.setFriends(new ArrayList<>());
                List < User > user_friend = getListFriends(connection,id);
                user.setFriends(user_friend);
                users.put(user.getId(),user);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return users.values();
    }

    @Override
    public User save(User entity) {

        if( entity == null )
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);

        int rowCount = 0;
        String sql = "insert into users (first_name,last_name) values ( ? , ? ) ";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql ) ) {

            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            rowCount = statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if(rowCount == 0)
            return entity;
        return null;
    }

    @Override
    public User delete(Long id) {
        if( id == null )
            throw new IllegalArgumentException("Id must be not null!");
        User user = findOne(id);
        if(user == null)
            return null;
        String sql = " delete from users where id = ? ";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql )   ) {


            String sqlDeleteFriendship = " delete from friendships where id_user1 = ? or id_user2 = ? ";
            PreparedStatement statement_delete = connection.prepareStatement(sqlDeleteFriendship);
            statement_delete.setLong(1,user.getId());
            statement_delete.setLong(2,user.getId());
            statement_delete.executeUpdate();


            statement.setLong(1,id);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return user;
    }

    @Override
    public User update(User entity) {
        if( entity == null )
            throw new IllegalArgumentException("Entity must be not null!");
        validator.validate(entity);

        User find_user = findOne(entity.getId());
        if( find_user == null )
            return entity;

        String sql = " update users set first_name = ?,last_name = ? where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement( sql )) {

            statement.setString(1,entity.getFirstName());
            statement.setString(2,entity.getLastName());
            statement.setLong(3,entity.getId());
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
