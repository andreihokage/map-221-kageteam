package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.Entity;
import ro.ubbcluj.map.domain.validators.Validator;
import ro.ubbcluj.map.repository.memory.InMemoryRepository;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractFileRepository< ID,E extends Entity<ID> > extends InMemoryRepository<ID,E> {

    private String filename;

    public AbstractFileRepository(String filename, Validator<E> validator) {
        super(validator);
        this.filename = filename;
        loadData();
    }

    /**
     * load all data from file in memory
     */
    private void loadData(){
        try(BufferedReader br = new BufferedReader( new FileReader(filename) ) ) {
            String line;
            while( ( line = br.readLine() ) != null ){
                if(line.equals(""))
                    continue;
                List< String > attributes = Arrays.asList(line.split(";"));
                E entity = extractEntity(attributes);
                super.save(entity);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param entity - append entity to the specific file
     */
    private void writeToFile(E entity){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(filename,true))){
            bw.write(createEntityAsString(entity));
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * save all data from memory to file
     */
    private void writeToFile(){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(filename,false))){

            for(E entity: super.findAll()){
                bw.write(createEntityAsString(entity));
                bw.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *  extract entity  - template method design pattern
     *  creates an entity of type E having a specified list of @code attributes
     * @param attributes
     * @return an entity of type E
     */
    protected abstract E extractEntity(List < String > attributes);

    /**
     *
     * @param entity
     * @return entity-specific string
     */
    protected abstract String createEntityAsString(E entity);

    @Override
    public E save(E entity){
        if( super.save(entity) == null ){
            writeToFile(entity);
            return null;
        }
        return entity;
    }

    @Override
    public E delete(ID id){
        E delete_entity = super.delete(id);
        if( delete_entity != null ){
            writeToFile();
            return delete_entity;
        }
        return null;
    }

    @Override
    public E update(E entity){
        E update_entity = super.update(entity);
        if( update_entity == null ){
            writeToFile();
            return null;
        }
        return update_entity;
    }
}
