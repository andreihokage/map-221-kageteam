package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.Validator;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.utils.Constants;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class FriendshipFile extends AbstractFileRepository< Pair < Long > , Friendship>{



    public FriendshipFile(String filename, Validator<Friendship> validator) {
        super(filename, validator);
    }

    @Override
    protected Friendship extractEntity(List<String> attributes) {
      //  Long id1 = Long.parseLong(attributes.get(0));
        Long idUserS = Long.parseLong(attributes.get(0));
        Long idUserD = Long.parseLong(attributes.get(1));
        DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate dateTime = LocalDate.parse(attributes.get(2), DATEFORMATTER );
        Friendship friendship = new Friendship(idUserS , idUserD , dateTime);
       // friendship.setId(id);
        return friendship;

    }

    @Override
    protected String createEntityAsString(Friendship entity) {
        String line = entity.getIdUserS() + ";" + entity.getIdUserD() + ";" + entity.getDate();
        return line;
    }
}
