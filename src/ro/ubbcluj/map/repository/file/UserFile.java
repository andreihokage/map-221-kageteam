package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.Validator;


import java.util.List;

public class UserFile extends AbstractFileRepository<Long, User>{
    public UserFile(String filename, Validator<User> validator) {
        super(filename, validator);
    }

    @Override
    protected User extractEntity(List<String> attributes) {
        User user = new User( attributes.get(1) , attributes.get(2) );
        user.setId( Long.parseLong( attributes.get(0) ) );
        return user;
    }

    @Override
    protected String createEntityAsString(User entity) {
        String line = entity.getId() + ";" + entity.getFirstName() + ";" + entity.getLastName();
        return line;
    }
}
