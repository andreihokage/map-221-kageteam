package ro.ubbcluj.map.repository.memory;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.validators.Validator;

public class FriendshipInMemoryRepository extends InMemoryRepository< Pair < Long >, Friendship> {
    public FriendshipInMemoryRepository(Validator<Friendship> validator) {
        super(validator);
    }
}
