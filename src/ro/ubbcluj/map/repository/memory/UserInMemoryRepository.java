package ro.ubbcluj.map.repository.memory;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.Validator;

public class UserInMemoryRepository extends InMemoryRepository<Long, User>{
    public UserInMemoryRepository(Validator<User> validator) {
        super(validator);
    }

}
