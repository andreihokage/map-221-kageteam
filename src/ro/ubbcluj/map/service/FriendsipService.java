package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.Exceptions.ErrorAddFriendship;
import ro.ubbcluj.map.domain.Exceptions.ErrorFindUser;
import ro.ubbcluj.map.domain.Exceptions.ErrorRemoveFriendship;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendsipService {

    private Repository<Pair< Long>, Friendship > repoFriendship;
    private Repository< Long , User > repoUser;

    public FriendsipService(Repository< Long , User > repoUser,Repository<Pair<Long>, Friendship> repoFriendship) {
        this.repoUser = repoUser;
        this.repoFriendship = repoFriendship;
    }

    /**
     * Method creates a friendship for the users:id,idFriend
     * @param id - id of the first user
     * @param idFriend - id of the second user
     */
    public void addFriendship(Long id, Long idFriend){
        if( repoUser.findOne(id) == null || repoUser.findOne(idFriend) == null )
            throw new ErrorFindUser("The specified users don't exist! ");
        Friendship friendship = new Friendship(id, idFriend, LocalDate.now());
        Friendship saveFriendship = repoFriendship.save(friendship);
        if( saveFriendship != null )
            throw new ErrorAddFriendship("The friendship already exist!");
    }

    /**
     *Method removes the friendship for the users:id,idFriend
     * @param id - id of the first user
     * @param idFriend - id of the second user
     */
    public void removeFriendship(Long id, Long idFriend){
        if( repoUser.findOne(id) == null || repoUser.findOne(idFriend) == null )
            throw new ErrorFindUser("The specified users don't exist! ");
        Friendship friendship = repoFriendship.delete( new Pair<Long>(id,idFriend) );
        if( friendship == null )
            throw new ErrorRemoveFriendship("The specified friendship doesn't exist!");
    }

    /**
     *
     * @return all friendships
     */
    public Iterable<Friendship> findAllFriendships(){
        return repoFriendship.findAll();
    }
}
