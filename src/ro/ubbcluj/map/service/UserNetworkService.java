package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.UserNetwork;
import ro.ubbcluj.map.repository.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserNetworkService {

    Repository < Long, User> repoUser;

    public UserNetworkService(Repository<Long, User> repoUser) {
        this.repoUser = repoUser;
    }

    /**
     *
     * @param user - set the user's list of friendships
     */
    private void setNeighboursVertices(User user){
        User new_user = repoUser.findOne(user.getId());
        user.setFriends(new_user.getFriends());
    }

    /**
     *
     * @param start - start node
     * @param viz - the color of nodes
     * @return the connected component of the node start
     */
    private UserNetwork ConnectedComponent(User start,Map< Long , Long > viz){
        List< User > queue = new ArrayList<User>();
        List< User > ans = new ArrayList<User>();

        queue.add(start);
        viz.put(start.getId(), 1L);
        while(queue.size() > 0){
            User vertex = queue.get(0);
            setNeighboursVertices(vertex);
            queue.remove(0);
            viz.put(vertex.getId(),2L);
            ans.add(vertex);

            for(User neighVertex : vertex.getFriends())
            if( viz.get( neighVertex.getId() ).equals(0L) ){
                viz.put( neighVertex.getId() , 1L );
                queue.add(neighVertex);
            }
        }
        return new UserNetwork(ans);
    }

    /**
     *
     * @return number of communities of social network
     */
    public int countNumberCommunities(){
        int ans = 0;
        Map < Long , Long > viz = new HashMap<Long,Long>();
        for(User x : repoUser.findAll())
            viz.put( x.getId() , 0L );
        for(User x : repoUser.findAll())
        if(  viz.get( x.getId() ).equals(0L) ){
            ConnectedComponent(x,viz);
            ans++;
        }
        return ans;
    }

    /**
     *
     * @param node - start node
     * @param viz - color of nodes
     * @param dist - the longest path in the subtree of node 'node'
     * @return longest path for the original tree
     */
    private Long DFS(User node,Map < Long,Long > viz,Map < Long,Long > dist){
        setNeighboursVertices(node);
        viz.put(node.getId(), 1L);
        dist.put(node.getId(), 0L);
        Long maxi1 = 0L , maxi2 = 0L;
        boolean ok = false;
        for(User neighVertex : node.getFriends())
            if ( viz.get(neighVertex.getId()).equals(0L) ) {
                ok = true;
                Long lengthBranch = DFS(neighVertex, viz,dist);
                if( lengthBranch > maxi1 ){
                    maxi2 = maxi1;
                    maxi1 = lengthBranch;
                }
                else if( lengthBranch >= maxi2 ){
                    maxi2 = lengthBranch;
                }
            }

        if( ok == false )
            return 0L;

        Long compDist = maxi1 + 1L; //the longest branch from node's subtree
        if( node.getFriends().size() > 1 && maxi1 + maxi2 + 2L > compDist )
            compDist = maxi1 + maxi2 + 2L;
        dist.put(node.getId(), compDist);
        viz.put(node.getId(),2L);
        return 1L + maxi1;
    }

    /**
     *
     * @param userNetwork - a userNetwork
     * @return longest path for a connected component
     */
    private Long computeLongestPathCommunity(UserNetwork userNetwork){
        int numberEdges = 0;
        for(User user : userNetwork.getListUsers() )
            numberEdges += user.getFriends().size();
        numberEdges /= 2;

        //userNetwork is an undirected connected component => userNetwork is a tree if it checks the below equality
        if( numberEdges != userNetwork.numberOfUsers() - 1 )
            return -1L;

        Map < Long,Long > viz = new HashMap<Long,Long>();
        Map < Long,Long > dist = new HashMap<Long,Long>();
        for(User user : userNetwork.getListUsers()) {
            dist.put(user.getId(),0L);
            viz.put(user.getId(), 0L);
        }
        DFS(userNetwork.getListUsers().get(0),viz,dist);
        Long ans = 0L;
        for(User user : userNetwork.getListUsers())
            if( dist.get( user.getId() ) > ans )
                ans = dist.get( user.getId() );
        return ans;
    }

    /**
     *
     * @return the component that contains the longest path
     */
    public UserNetwork computeLongestPath(){
        Long ans = 0L;
        UserNetwork ansNetwork = new UserNetwork( new ArrayList<User>() );
        Map < Long,Long > viz = new HashMap<Long,Long>();
        for(User user : repoUser.findAll())
            viz.put( user.getId() , 0L );
        for(User user : repoUser.findAll())
        if( viz.get( user.getId() ).equals(0L) ){
            UserNetwork userNetwork = ConnectedComponent( user , viz );
            Long lengthPath = computeLongestPathCommunity(userNetwork);
            if( ans <= lengthPath  ) {
                ans = lengthPath;
                ansNetwork = userNetwork;
            }
        }
        return  ansNetwork;
    }
}
