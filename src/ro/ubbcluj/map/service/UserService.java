package ro.ubbcluj.map.service;

import ro.ubbcluj.map.domain.Exceptions.*;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;

import java.time.LocalDateTime;

public class UserService {

    private Repository<Long, User> repoUser;

    public UserService(Repository<Long, User> repoUser) {
        this.repoUser = repoUser;
    }

    /**
     * Method creates a user
     * @param firstName - user's firstname
     * @param lastName -  user's lastname
     */
    public void addUser(String firstName,String lastName)  {
        User user = new User(firstName,lastName);
        User saveUser = repoUser.save(user);
        if ( saveUser != null )
            throw new ErrorAddUser("The user is already saved!");
    }

    /**
     * Method removes the user with the id:id
     * @param id - removed user's id
     */
    public void removeUser(Long id){
        User removeUser = repoUser.delete(id);
        if ( removeUser == null )
            throw new ErrorDeleteUser("The user does not exist!");
    }

    /**
     *
     * @return all users
     */
    public Iterable<User> findAllUsers(){
        return repoUser.findAll();
    }
}
