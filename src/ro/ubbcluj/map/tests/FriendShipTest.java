package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.User;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendShipTest {

    public static void runFriendShip(){
        User user1 = new User("Andrei","Balanici");
        User user2 = new User("Madara","Uchiha");
        User user3 = new User("Naruto","Uzumaky");
        Friendship friendship1 = new Friendship(1L , 2L , LocalDate.now() );
        assert ( friendship1.getIdUserS().equals(1L) );
        assert ( friendship1.getIdUserD().equals(2L)  );

    }
}
