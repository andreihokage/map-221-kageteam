package ro.ubbcluj.map.tests;

import jdk.jfr.Frequency;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipValidator;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.RepositoryFactory;
import ro.ubbcluj.map.repository.StrategyPersistency;
import ro.ubbcluj.map.repository.StrategyRepository;
import ro.ubbcluj.map.repository.file.FriendshipFile;
import ro.ubbcluj.map.repository.file.UserFile;


import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

public class FriendshipFileTest {

    private static void cleanFiles(){
        try (PrintWriter writer = new PrintWriter("datatest/userfiletest.in")) {
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (PrintWriter writer = new PrintWriter("datatest/friendshiptest.in")) {
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void runSave(){

        cleanFiles();

        try (BufferedWriter bw = new BufferedWriter(new FileWriter("datatest/friendshiptest.in", true))) {
            bw.write("1;3;10/31/2021");
            bw.newLine();
            bw.write("2;3;10/31/2021");
            bw.newLine();
            bw.write("4;3;10/31/2021");
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Repository< Pair < Long >, Friendship > repoFriend = new FriendshipFile("datatest/friendshiptest.in",new FriendshipValidator());
        Repository< Pair < Long > , Friendship > repoFriend = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.FRIENDSHIP);
        Friendship friendship = new Friendship(3L,1L, LocalDate.now());
        assert repoFriend.save(friendship).equals(friendship);
        Friendship friendship2 = new Friendship(1L,2L, LocalDate.now());
        assert repoFriend.save(friendship2) == null;
        //assert Arrays.asList( repoFriend.findAll() ).size() == 4;

        assert repoFriend.delete(new Pair<Long>(3L,1L)).equals(friendship);
        assert repoFriend.delete(friendship.getId()) == null;

        Friendship new_friend = new Friendship( 4L,5L,LocalDate.now());
        new_friend.setId(friendship2.getId());
        assert repoFriend.update(new_friend) == null;

        cleanFiles();
    }

    public static void runFriendFile(){
        runSave();
    }
}
