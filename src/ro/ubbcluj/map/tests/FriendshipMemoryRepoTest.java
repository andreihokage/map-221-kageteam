package ro.ubbcluj.map.tests;

import jdk.jfr.Frequency;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipValidator;
import ro.ubbcluj.map.domain.validators.ValidationException;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.RepositoryFactory;
import ro.ubbcluj.map.repository.StrategyPersistency;
import ro.ubbcluj.map.repository.StrategyRepository;
import ro.ubbcluj.map.repository.memory.FriendshipInMemoryRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendshipMemoryRepoTest {

    private static void runSave(){
        //Repository< Pair< Long >, Friendship> repository = new FriendshipInMemoryRepository(new FriendshipValidator());
        Repository< Pair < Long > , Friendship > repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY, StrategyRepository.FRIENDSHIP);
        // User user2 = new User("Madara","Uchiha",30);
        // User user3 = new User("Naruto","Uzumaky",10);
        Friendship friendship2 = new Friendship(2L , 3L, LocalDate.now());
        Friendship friendship3 = new Friendship(3L , 2L, LocalDate.now());

        try{
            repository.save(null);
            assert false;
        }catch (IllegalArgumentException e){
            assert ( e.getMessage().equals("Entity must be not null!") );
        }

        assert repository.save(friendship2) == null;
        assert repository.save(friendship2).equals(friendship2);
    }

    private static void runDelete(){
        //Repository< Pair< Long > , Friendship> repository = new FriendshipInMemoryRepository(new FriendshipValidator());
        Repository< Pair < Long > , Friendship > repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY, StrategyRepository.FRIENDSHIP);
        //User user2 = new User("Madara","Uchiha",30);
        //User user3 = new User("Naruto","Uzumaky",10);
        Friendship friendship2 = new Friendship(2L , 3L, LocalDate.now());

        try{
            repository.delete(null);
            assert false;
        }catch(IllegalArgumentException e){
            assert e.getMessage().equals("Id must be not null!");
        }

        repository.save(friendship2);
        assert repository.delete(friendship2.getId()).equals(friendship2);
        assert repository.delete(friendship2.getId()) == null;
    }

    private static void runFindOne(){
        // Repository<  Pair< Long >, Friendship> repository = new FriendshipInMemoryRepository(new FriendshipValidator());
        Repository< Pair < Long > , Friendship > repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY, StrategyRepository.FRIENDSHIP);
        //User user2 = new User("Madara","Uchiha",30);
        // User user3 = new User("Naruto","Uzumaky",10);
        Friendship friendship2 = new Friendship(2L , 3L, LocalDate.now());

        assert repository.findOne(friendship2.getId()) == null;
        repository.save(friendship2);
        assert repository.findOne(friendship2.getId()).equals(friendship2);
        try{
            repository.findOne(null);
            assert false;
        }catch (IllegalArgumentException e){
            assert e.getMessage().equals("Id must be not null!");
        }
    }

    private static void runUpdate() {
        //Repository< Pair< Long > , Friendship> repository = new FriendshipInMemoryRepository(new FriendshipValidator());
        Repository< Pair < Long > , Friendship > repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY, StrategyRepository.FRIENDSHIP);
        // User user1 = new User("Madara", "Uchiha", 1);
        // User user2 = new User("Madara", "Uchiha", 30);
        // User user3 = new User("Naruto", "Uzumaky", 10);
        Friendship friendship2 = new Friendship( 2L , 3L, LocalDate.now());
        repository.save(friendship2);
        Friendship friendship3 = new Friendship( 1L , 3L, LocalDate.now());
        friendship3.setId(friendship2.getId());
        assert repository.update(friendship3) == null;
        Friendship find_fr = repository.findOne(friendship2.getId());
        assert find_fr.getIdUserS().equals(1L);
        assert find_fr.getIdUserD().equals(3L);

        Friendship friendship = new Friendship( 1L , 2L, LocalDate.now());
        friendship.setId(new Pair(-1L,-1L) );
        assert repository.update(friendship).equals(friendship);

        try{
            repository.update(null);
            assert false;
        }catch (IllegalArgumentException e){
            assert e.getMessage().equals("Entity must be not null!");
        }

    }

    public static void runRepo(){
        runSave();
        runDelete();
        runFindOne();
        runUpdate();
    }
}
