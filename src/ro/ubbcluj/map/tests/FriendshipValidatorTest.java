package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipException;
import ro.ubbcluj.map.domain.validators.FriendshipValidator;
import ro.ubbcluj.map.domain.validators.ValidationException;
import ro.ubbcluj.map.domain.validators.Validator;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendshipValidatorTest {

    public static void runValidator(){
        Validator<Friendship> validator = new FriendshipValidator();
        Friendship friendship2 = new Friendship(2L , 3L, LocalDate.now());
        try{
            validator.validate(friendship2);
            assert true;
        }catch(FriendshipException e){
            assert false;
        }

        Friendship friendship3 = new Friendship(-2L , -2L, LocalDate.now());
        try{
            validator.validate(friendship3);
            assert false;
        }catch(FriendshipException e){
            assert e.getMessage().equals("A user cannot be friend of himself!User1 invalid!User2 invalid!");
        }
    }

}
