package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.tests.dbtests.FriendshipDbRepoTest;
import ro.ubbcluj.map.tests.dbtests.NetworkDb;
import ro.ubbcluj.map.tests.dbtests.UserDbRepoTest;
import ro.ubbcluj.map.tests.dbtests.UserServiceDbTest;

public class Tests {

    public void run_all_tests(){
        UserTest.runUser();
        FriendShipTest.runFriendShip();
        UserValidatorTest.runUserValidator();
        FriendshipValidatorTest.runValidator();
        UserInMemortRepoTest.runRepo();
        FriendshipMemoryRepoTest.runRepo();
        UserFileTest.runUserFile();
        FriendshipFileTest.runFriendFile();
       // UserServiceFileTest.runService();
       // UserNetworkServiceTest.runNetworkService();
        UserDbRepoTest.runUserDbTests();
        FriendshipDbRepoTest.runFriendDbRepoTest();
        UserServiceDbTest.runUserServiceDbTest();
        NetworkDb.runNetworkDb();
    }
}
