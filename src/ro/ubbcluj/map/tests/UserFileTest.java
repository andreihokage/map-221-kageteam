package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.RepositoryFactory;
import ro.ubbcluj.map.repository.StrategyPersistency;
import ro.ubbcluj.map.repository.StrategyRepository;
import ro.ubbcluj.map.repository.file.UserFile;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserFileTest {

    private static void cleanFiles(){
        try (PrintWriter writer = new PrintWriter("datatest/userfiletest.in")) {
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void runSave(){

        cleanFiles();
        //Repository<Long, User> repository = new UserFile("datatest/userfiletest.in",new UserValidator());
        Repository < Long , User> repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.USER);
        User user1 = new User("Andrei","Balanici");
        user1.setId(1L);
        User user2 = new User("Madara","Uchiha");
        user2.setId(2L);
        User user3 = new User("Naruto","Uzumaky");
        user3.setId(3L);
        assert repository.save(user1) == null;
        assert repository.save(user2) == null;
        assert repository.save(user3) == null;

        assert repository.save(user3).equals(user3);
        assert repository.save(user3).equals(user3);

        User find_user2 = repository.findOne(user2.getId());
        assert find_user2.equals(user2);
        assert repository.delete(user2.getId()).equals(user2);
        assert repository.delete(user2.getId()) == null;

        User new_user = new User("Naruto","Namikaze");
        new_user.setId(user3.getId());
        assert repository.update(new_user) == null;
        new_user.setId(-1L);
        assert repository.update(new_user).equals(new_user);
        User find_user3 = repository.findOne(user3.getId());
        assert find_user3.getLastName().equals("Namikaze");

        cleanFiles();
    }

    public static void runUserFile(){
        runSave();
    }

}
