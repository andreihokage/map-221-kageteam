package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.domain.validators.ValidationException;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.RepositoryFactory;
import ro.ubbcluj.map.repository.StrategyPersistency;
import ro.ubbcluj.map.repository.StrategyRepository;
import ro.ubbcluj.map.repository.memory.UserInMemoryRepository;

import java.util.List;

public class UserInMemortRepoTest {

    private static void runSave(){
       // Repository < Long , User> repository = new UserInMemoryRepository(new UserValidator());
        Repository < Long , User> repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY, StrategyRepository.USER);
        User user1 = new User("Andrei","Balanici");
        user1.setId(1L);
        User user2 = new User("Madara","Uchiha");
        user2.setId(2L);
        User user3 = new User("Naruto","");
        user3.setId(3L);

        assert ( repository.save(user1) == null );
        assert ( repository.save(user2) == null );
        assert ( repository.save(user1).equals(user1) );

        try{
            repository.save(user3);
            assert (false);
        }catch(ValidationException e){
            assert( e.getMessage().equals("Last name is empty!") );
        }

        User user4 = null;
        try{
            repository.save(user4);
            assert false;
        }catch(IllegalArgumentException e){
            assert ( e.getMessage().equals("Entity must be not null!") );
        }
    }

    private static void runDelete(){
        //Repository < Long , User> repository = new UserInMemoryRepository(new UserValidator());
        Repository < Long ,User > repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY,StrategyRepository.USER);
        User user1 = new User("Andrei","Balanici");
        user1.setId(1L);
        User user2 = new User("Madara","Uchiha");
        user2.setId(2L);
        User user3 = new User("Naruto","Uzumaky");
        user3.setId(3L);

        assert ( repository.save(user1) == null );
        assert ( repository.save(user2) == null );
        assert ( repository.save(user3) == null );

        try{
            repository.delete(null);
            assert false;
        }catch(IllegalArgumentException e){
            assert ( e.getMessage().equals("Id must be not null!") );
        }

        assert ( repository.delete(user2.getId()).equals(user2) );
        assert ( repository.delete(user2.getId()) == null );

    }

    private static void runUpdate(){
       // Repository < Long , User> repository = new UserInMemoryRepository(new UserValidator());
        Repository < Long , User> repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY, StrategyRepository.USER);
        User user1 = new User("Andrei","Balanici");
        user1.setId(1L);
        User user2 = new User("Madara","Uchiha");
        user2.setId(2L);
        User user3 = new User("Naruto","Uzumaky");
        user3.setId(3L);

        assert ( repository.save(user1) == null );
        assert ( repository.save(user2) == null );
        assert ( repository.save(user3) == null );

        try{
            repository.update(null);
            assert false;
        }catch(IllegalArgumentException e){
            assert ( e.getMessage().equals("Entity must be not null!") );
        }

        User user5 = new User("a","");
        user5.setId(1L);
        try{
            repository.update(user5);
            assert false;
        }catch(ValidationException e){
            assert ( e.getMessage().equals("Last name is empty!") );
        }

        User user4 = new User("Kaguya","Balanici");
        user4.setId(user1.getId());
        assert ( repository.update(user4) == null );
        User find_user4 = repository.findOne(user1.getId());

        assert ( find_user4.getFirstName().equals("Kaguya") );
        assert ( find_user4.getLastName().equals("Balanici") );


        user4.setId(-2L);
        assert ( repository.update(user4).equals(user4) );
    }

    private static void runFindOne(){
        //Repository < Long , User> repository = new UserInMemoryRepository(new UserValidator());
        Repository < Long , User> repository = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.MEMORY, StrategyRepository.USER);
        User user1 = new User("Andrei","Balanici");
        user1.setId(1L);
        User user2 = new User("Madara","Uchiha");
        user2.setId(2L);
        User user3 = new User("Naruto","Uzumaky");
        user3.setId(3L);

        assert ( repository.save(user1) == null );
        assert ( repository.save(user2) == null );
        assert ( repository.save(user3) == null );

        try{
            repository.update(null);
            assert false;
        }catch(IllegalArgumentException e){
            assert ( e.getMessage().equals("Entity must be not null!") );
        }

        assert ( repository.findOne(user1.getId()).equals(user1) );
        assert ( repository.findOne(-1L) == null );
    }

    public static void runRepo(){
        runSave();
        runUpdate();
        runDelete();
        runFindOne();
    }

}
