package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.UserNetwork;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.RepositoryFactory;
import ro.ubbcluj.map.repository.StrategyPersistency;
import ro.ubbcluj.map.repository.StrategyRepository;
import ro.ubbcluj.map.service.FriendsipService;
import ro.ubbcluj.map.service.UserNetworkService;
import ro.ubbcluj.map.service.UserService;

import java.io.*;

public class UserNetworkServiceTest {

    private static void cleanFiles(){
        try (PrintWriter writer = new PrintWriter("datatest/userfiletest.in")) {
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (PrintWriter writer = new PrintWriter("datatest/friendshiptest.in")) {
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void writeInFiles(){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("datatest/userfiletest.in",true))) {
            bw.write("1;Andrei;Balanici");
            bw.newLine();
            bw.write("2;Madara;Uchiha");
            bw.newLine();
            bw.write("3;Naruto;Uzumaky");
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter("datatest/friendshiptest.in",true))) {
            bw.write("1;2;10/31/2021");
            bw.newLine();
            bw.write("1;3;10/31/2021");
            bw.newLine();
            bw.write("2;3;10/31/2021");
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void runNumberConnected(){
        cleanFiles();
        //Repository<Long, User> repoUser = new UserFile("datatest/userfiletest.in",new UserValidator()) ;
        //Repository<Pair< Long >, Friendship> repoFriend = new FriendshipFile("datatest/friendshiptest.in",new FriendshipValidator());
        Repository < Long , User> repoUser = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.USER);
        Repository< Pair < Long > , Friendship > repoFriend = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.FRIENDSHIP);
        UserService userService = new UserService(repoUser);
        FriendsipService friendsipService = new FriendsipService(repoUser,repoFriend);
        UserNetworkService userNetwork = new UserNetworkService(repoUser);

        userService.addUser("aa1","bb1");
        userService.addUser("cc2","dd2");
        userService.addUser("aa3","bb3");
        userService.addUser("aa4","bb4");
        userService.addUser("aa5","bb5");
        userService.addUser("aa6","bb6");
        userService.addUser("aa7","bb7");
        userService.addUser("aa8","bb8");
        userService.addUser("aa9","bb9");

        assert userNetwork.countNumberCommunities() == 9;
        //first component
        friendsipService.addFriendship(1L,2L);
        friendsipService.addFriendship(1L,3L);
        friendsipService.addFriendship(2L,3L);
        assert userNetwork.countNumberCommunities() == 7;
        //second component
        friendsipService.addFriendship(4L,5L);
        assert userNetwork.countNumberCommunities() == 6;
        //third componnet
        friendsipService.addFriendship(9L,8L);
        friendsipService.addFriendship(8L,7L);
        friendsipService.addFriendship(7L,6L);
        friendsipService.addFriendship(9L,6L);
        assert userNetwork.countNumberCommunities() == 3;
        friendsipService.addFriendship(6L,5L);
        assert userNetwork.countNumberCommunities() == 2;
        friendsipService.addFriendship(8L,1L);
        assert userNetwork.countNumberCommunities() == 1;

        cleanFiles();
    }

    private static void runLongestPath(){
        cleanFiles();
        writeInFiles();

        Repository < Long , User> repoUser = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.USER);
        Repository< Pair < Long > , Friendship > repoFriend = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.FRIENDSHIP);
        UserService userService = new UserService(repoUser);
        FriendsipService friendsipService = new FriendsipService(repoUser,repoFriend);
        UserNetworkService serviceNetwork = new UserNetworkService(repoUser);

        UserNetwork userNetwork = serviceNetwork.computeLongestPath();
        assert  userNetwork.numberOfUsers() == 0;

        userService.addUser("aa1","bb1");
        userService.addUser("cc2","dd2");
        userService.addUser("aa3","bb3");
        userService.addUser("aa4","bb4");
        userService.addUser("aa5","bb5");
        userService.addUser("aa6","bb6");
        userService.addUser("aa7","bb7");
        userService.addUser("aa8","bb8");
        userService.addUser("aa9","bb9");

        friendsipService.addFriendship(4L,5L);
        //1-2-3-1;4-5;6;7;8;9;10;11;12
        UserNetwork userNetwork1 = serviceNetwork.computeLongestPath();
        assert  userNetwork1.numberOfUsers() == 2;

        friendsipService.addFriendship(6L,5L);
        friendsipService.addFriendship(4L,6L);
        //1-2-3-1;4-5-6-4;7;8;9;10;11;12
        UserNetwork userNetwork2 = serviceNetwork.computeLongestPath();
        assert  userNetwork2.numberOfUsers() == 1;

        friendsipService.addFriendship(8L,7L);
        friendsipService.addFriendship(10L,8L);
        friendsipService.addFriendship(11L,8L);
        friendsipService.addFriendship(7L,9L);
        friendsipService.addFriendship(9L,12L);

        UserNetwork userNetwork3 = serviceNetwork.computeLongestPath();
        assert  userNetwork3.numberOfUsers() == 6;


        cleanFiles();
    }

    public static void runNetworkService(){
        runNumberConnected();
        runLongestPath();
    }

}
