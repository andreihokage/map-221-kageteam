package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.Exceptions.ErrorAddFriendship;
import ro.ubbcluj.map.domain.Exceptions.ErrorDeleteUser;
import ro.ubbcluj.map.domain.Exceptions.ErrorFindUser;
import ro.ubbcluj.map.domain.Exceptions.ErrorRemoveFriendship;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.RepositoryFactory;
import ro.ubbcluj.map.repository.StrategyPersistency;
import ro.ubbcluj.map.repository.StrategyRepository;
import ro.ubbcluj.map.service.FriendsipService;
import ro.ubbcluj.map.service.UserService;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class UserServiceFileTest {

    private static void cleanFiles(){
        try (PrintWriter writer = new PrintWriter("datatest/userfiletest.in")) {
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (PrintWriter writer = new PrintWriter("datatest/friendshiptest.in")) {
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void writeInFiles(){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("datatest/userfiletest.in",true))) {
            bw.write("1;Andrei;Balanici");
            bw.newLine();
            bw.write("2;Madara;Uchiha");
            bw.newLine();
            bw.write("3;Naruto;Uzumaky");
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter("datatest/friendshiptest.in",true))) {
            bw.write("1;2;10/31/2021");
            bw.newLine();
            bw.write("1;3;10/31/2021");
            bw.newLine();
            bw.write("2;3;10/31/2021");
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void runAddUser(){
        cleanFiles();
        writeInFiles();
        Repository < Long , User> repoUser = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.USER);
        Repository< Pair < Long > , Friendship > repoFriend = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.FRIENDSHIP);
        UserService userService = new UserService(repoUser);

        userService.addUser("Kushina","Namikaze");
        userService.addUser("Kushina","Namikaze2");
        User find_user1 = repoUser.findOne(4L);
        assert find_user1.getFirstName().equals("Kushina");
        assert find_user1.getLastName().equals("Namikaze");

        repoUser.delete(2L);
        userService.addUser("aa","bb");
        assert repoUser.findOne(6L).getId().equals(6L);
        assert repoUser.findOne(6L).getFirstName().equals("aa");
        cleanFiles();
    }

    private static void runRemoveUser(){
        cleanFiles();
        writeInFiles();

        Repository < Long , User> repoUser = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.USER);
        Repository< Pair < Long > , Friendship > repoFriend = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.FRIENDSHIP);
        UserService userService = new UserService(repoUser);

        User user1 = repoUser.findOne(1L);
        User user2 = repoUser.findOne(2L);
        User user3 = repoUser.findOne(3L);

        //assert Arrays.asList(repoFriend.findAll()).size() == 3;
        assert repoFriend.findOne(new Pair<Long>(2L,1L)) != null;
        assert repoFriend.findOne(new Pair<Long>(3L,1L)) != null;
        userService.removeUser(1L);
        //assert Arrays.asList(repoFriend.findAll()).size() == 1;
        assert repoFriend.findOne(new Pair<Long>(2L,1L)) == null;
        assert repoFriend.findOne(new Pair<Long>(3L,1L)) == null;

        try{
            userService.removeUser(100L);
            assert false;
        }catch (ErrorDeleteUser e){
            assert e.getMessage().equals("The user does not exist!");
        }

        cleanFiles();
    }

    private static void runAddFriendship(){
        cleanFiles();
        writeInFiles();

        Repository < Long , User> repoUser = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.USER);
        Repository< Pair < Long > , Friendship > repoFriend = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.FRIENDSHIP);
        UserService userService = new UserService(repoUser);
        FriendsipService friendsipService = new FriendsipService(repoUser,repoFriend);
        User user1 = repoUser.findOne(1L);
        User user2 = repoUser.findOne(2L);
        User user3 = repoUser.findOne(3L);

        List< User > l1 = user1.getFriends();
        assert l1.size() == 2;
        assert l1.get(0).equals(user2);
        assert l1.get(1).equals(user3);
        List< User > l2 = user2.getFriends();
        assert l2.size() == 2;
        assert l2.get(0).equals(user1);
        assert l2.get(1).equals(user3);

        userService.addUser("aa","bb");
        assert repoFriend.findOne(new Pair<Long>(4L,3L)) == null;
        friendsipService.addFriendship(4L,3L);
        assert repoFriend.findOne(new Pair<Long>(4L,3L)) != null;

        List< User > l3 = user3.getFriends();
        assert l3.size() == 3;
        assert l3.get(0).equals(user2);
        assert l3.get(1).equals(user1);
        assert l3.get(2).getFirstName().equals("aa");

        try{
            friendsipService.addFriendship(4L,3L);
            assert false;
        }catch (ErrorAddFriendship e){
            assert e.getMessage().equals("The friendship already exist!");
        }

        try{
            friendsipService.addFriendship(300L,4L);
            assert false;
        }catch (ErrorFindUser e){
            assert e.getMessage().equals("User1 does not exist!");
        }

        cleanFiles();
    }

    private static void runRemoveFriendship(){
        cleanFiles();
        writeInFiles();

        Repository < Long , User> repoUser = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.USER);
        Repository< Pair < Long > , Friendship > repoFriend = RepositoryFactory.getInstance().createRepositoryTesting(StrategyPersistency.FILE, StrategyRepository.FRIENDSHIP);
        UserService userService = new UserService(repoUser);
        FriendsipService friendsipService = new FriendsipService(repoUser,repoFriend);
        User user1 = repoUser.findOne(1L);
        User user2 = repoUser.findOne(2L);
        User user3 = repoUser.findOne(3L);

        userService.addUser("aa","bb");
        assert repoFriend.findOne(new Pair<Long>(3L,2L)) != null;
        friendsipService.removeFriendship(3L,2L);
        assert repoFriend.findOne(new Pair<Long>(3L,2L)) == null;

        try{
            friendsipService.removeFriendship(3L,2L);
            assert false;
        }catch (ErrorRemoveFriendship e){
            assert e.getMessage().equals("The specified friendship doesn't exist!");
        }

        try{
            friendsipService.removeFriendship(300L,4L);
            assert false;
        }catch (ErrorFindUser e){
            assert e.getMessage().equals("User1 does not exist!");
        }

        cleanFiles();
    }

    public static void runService(){
        runAddUser();
        runRemoveUser();
        runAddFriendship();
        runRemoveFriendship();
    }
}
