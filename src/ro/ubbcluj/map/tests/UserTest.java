package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.User;

import java.util.List;

public class UserTest {

    public static void runUser(){
        User user1 = new User("Andrei","Balanici");
        User user2 = new User("Madara","Uchiha");
        User user3 = new User("Naruto","Uzumaky");

        assert ( user1.getFirstName() == "Andrei" );
        assert ( user1.getLastName() == "Balanici" );

        user1.addFriend(user2);
        user1.addFriend(user3);
        List< User > list = user1.getFriends();
        assert ( list.size() == 2 );
        assert ( list.get(1).equals(user3) );

        user1.removeFriend(user3);
        assert ( list.size() == 1 );
        assert ( list.get(0).equals(user2) );
    }

}
