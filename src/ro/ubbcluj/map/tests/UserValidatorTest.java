package ro.ubbcluj.map.tests;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.UserException;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.domain.validators.ValidationException;

public class UserValidatorTest {

    public static void runUserValidator(){
        User user = new User("","");
        user.setId(1L);
        UserValidator userValidator = new UserValidator();
        try{
            userValidator.validate(user);
            assert ( false );
        }catch(UserException e){
            assert ( e.getMessage().equals("First name is empty!Last name is empty!"));
        }

        User user1 = new User("a","a");
        user1.setId(1L);
        try{
            userValidator.validate(user1);
            assert ( true );
        }catch(UserException e){
            assert ( false );
        }
    }
}
