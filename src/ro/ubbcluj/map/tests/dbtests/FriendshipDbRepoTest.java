package ro.ubbcluj.map.tests.dbtests;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipDbValidator;
import ro.ubbcluj.map.domain.validators.UserDbValidator;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
import ro.ubbcluj.map.repository.db.UserDbRepository;
import ro.ubbcluj.map.utils.DbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FriendshipDbRepoTest {


    private static void clearDb(){
        String sql = " delete from friendships";
        String sql1 = " delete from users";
        try (Connection connection = DriverManager.getConnection(DbConnection.URLTEST, DbConnection.USERNAME, DbConnection.PASSWORD);
             PreparedStatement statement = connection.prepareStatement( sql );
             PreparedStatement statement1 = connection.prepareStatement( sql1 )){

            statement.executeUpdate();
            statement1.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void clearDbUsers(){
        String sql = " delete from users";
        try (Connection connection = DriverManager.getConnection(DbConnection.URLTEST, DbConnection.USERNAME, DbConnection.PASSWORD);
             PreparedStatement statement = connection.prepareStatement( sql )) {
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    private static void runSave(){
        clearDb();

        Repository<Long, User> repoUsers = new UserDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new UserDbValidator());
        User user1 = new User("andrei","balanici");
        User user2 = new User("mihai","coranyu");
        User user3 = new User("daniel","morjan");
        repoUsers.save(user1);
        repoUsers.save(user2);
        repoUsers.save(user3);

        List< Long > list = new ArrayList<>();
        for(User user :repoUsers.findAll())
            list.add(user.getId());


        Repository < Pair< Long >, Friendship > repoFriend = new FriendshipDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new FriendshipDbValidator());
        Friendship friendship1 = new Friendship(list.get(1),list.get(0), LocalDate.now());
        Friendship friendship2 = new Friendship(list.get(2),list.get(1), LocalDate.now());
        Friendship friendship3 = new Friendship(list.get(2),list.get(0), LocalDate.now());
        repoFriend.save(friendship1);
        repoFriend.save(friendship2);
        repoFriend.save(friendship3);

        Friendship find_friendship = repoFriend.findOne(new Pair<Long>( list.get(0),list.get(1) ));
        assert find_friendship.equals(friendship1);
        assert repoFriend.delete(friendship1.getId()).equals(friendship1);
        assert repoFriend.delete(friendship1.getId()) == null;

        clearDb();
    }

    public static void runFriendDbRepoTest(){
        runSave();
    }

}
