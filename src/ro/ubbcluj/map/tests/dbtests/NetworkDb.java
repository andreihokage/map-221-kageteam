package ro.ubbcluj.map.tests.dbtests;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipDbValidator;
import ro.ubbcluj.map.domain.validators.UserDbValidator;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
import ro.ubbcluj.map.repository.db.UserDbRepository;
import ro.ubbcluj.map.service.FriendsipService;
import ro.ubbcluj.map.service.UserNetworkService;
import ro.ubbcluj.map.service.UserService;
import ro.ubbcluj.map.utils.DbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NetworkDb {

    private static void clearDb(){
        String sql = " delete from friendships";
        String sql1 = " delete from users";
        try (Connection connection = DriverManager.getConnection(DbConnection.URLTEST, DbConnection.USERNAME, DbConnection.PASSWORD);
             PreparedStatement statement = connection.prepareStatement( sql );
             PreparedStatement statement1 = connection.prepareStatement( sql1 )){

            statement.executeUpdate();
            statement1.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void runNetwork(){
        clearDb();
        Repository<Long, User> repoUser = new UserDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new UserDbValidator());
        Repository <Pair< Long >, Friendship> repoFriend = new FriendshipDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new FriendshipDbValidator());
        UserService userService = new UserService(repoUser);
        FriendsipService friendsipService = new FriendsipService(repoUser,repoFriend);
        UserNetworkService userNetworkService = new UserNetworkService(repoUser);
        userService.addUser("andrei","balanici");
        userService.addUser("razvan","vass");
        userService.addUser("dani","morjan");
        userService.addUser("bakugan","maruto");

        List< Long > list = new ArrayList<>();
        for(User user :repoUser.findAll())
            list.add(user.getId());
        assert list.size() == 4;
        Long id0 = list.get(0);
        Long id1 = list.get(1);
        Long id2 = list.get(2);
        Long id3 = list.get(3);
        //0:1,2,3  1:0,3 2:0,3 3:0,1,2
        assert userNetworkService.countNumberCommunities() == 4;
        friendsipService.addFriendship(id0,id1);
        friendsipService.addFriendship(id2,id0);
        friendsipService.addFriendship(id3,id0);
        friendsipService.addFriendship(id2,id3);
        friendsipService.addFriendship(id1,id3);
        assert userNetworkService.countNumberCommunities() == 1;

        //I remove the user with id id3
        //0:1,2  1:0 2:0 4
        userService.removeUser(id3);
        userService.addUser("l","p");
        assert userNetworkService.countNumberCommunities() == 2;
        assert userNetworkService.computeLongestPath().numberOfUsers() == 3;
    }

    public static void runNetworkDb(){
        runNetwork();
    }
}
