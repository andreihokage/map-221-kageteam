package ro.ubbcluj.map.tests.dbtests;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Pair;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.domain.validators.FriendshipDbValidator;
import ro.ubbcluj.map.domain.validators.UserDbValidator;
import ro.ubbcluj.map.domain.validators.UserValidator;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
import ro.ubbcluj.map.repository.db.UserDbRepository;
import ro.ubbcluj.map.utils.DbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class UserDbRepoTest {

    private static void clearDb(){
        String sql = " delete from friendships";
        String sql1 = " delete from users";
        try (Connection connection = DriverManager.getConnection(DbConnection.URLTEST, DbConnection.USERNAME, DbConnection.PASSWORD);
             PreparedStatement statement = connection.prepareStatement( sql );
             PreparedStatement statement1 = connection.prepareStatement( sql1 )){

            statement.executeUpdate();
            statement1.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    private static void runSaveTest(){
        clearDb();
        Repository<Long, User> repository = new UserDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new UserDbValidator());
        User user1 = new User("andrei","balanici");
        User user2 = new User("mihai","coranyu");
        User user3 = new User("daniel","morjan");
        User user4 = new User("bogdan","bentea");
        user4.setId(0L);
        repository.save(user1);
        repository.save(user2);
        repository.save(user3);
        int cnt = 0;
        for(User user : repository.findAll()){
            cnt = cnt + 1;
            assert repository.findOne(user.getId()).equals(user);
        }
        assert cnt == 3;

        try{
            repository.save(null);
            assert false;
        }catch(IllegalArgumentException e){
            assert e.getMessage().equals("Entity must be not null!");
        }

        try{
            repository.delete(null);
            assert false;
        }catch(IllegalArgumentException e){
            assert e.getMessage().equals("Id must be not null!");
        }
        assert repository.delete(user4.getId()) == null;

        Long id_delete = 0L;
        for(User user : repository.findAll()){
            id_delete = user.getId();
            break;
        }
        User user_upd = new User("zz","yy");
        user_upd.setId(id_delete);
        assert repository.update(user_upd) == null;
        User find_user = repository.findOne(id_delete);
        assert find_user.getFirstName().equals("zz");
        assert find_user.getLastName().equals("yy");
        assert repository.delete(id_delete).equals(find_user);
        clearDb();
    }

    private static void runUserFriendTest(){
        clearDb();
        Repository<Long, User> repoUsers = new UserDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new UserDbValidator());
        User user1 = new User("andrei","balanici");
        User user2 = new User("mihai","coranyu");
        User user3 = new User("daniel","morjan");
        repoUsers.save(user1);
        repoUsers.save(user2);
        repoUsers.save(user3);

        List< Long > list = new ArrayList<>();
        for(User user :repoUsers.findAll())
            list.add(user.getId());


        Repository <Pair< Long >, Friendship> repoFriend = new FriendshipDbRepository(DbConnection.URLTEST,DbConnection.USERNAME,DbConnection.PASSWORD,new FriendshipDbValidator());
        Friendship friendship1 = new Friendship(list.get(1),list.get(0), LocalDate.now());
        Friendship friendship2 = new Friendship(list.get(2),list.get(1), LocalDate.now());
        Friendship friendship3 = new Friendship(list.get(2),list.get(0), LocalDate.now());
        repoFriend.save(friendship1);
        repoFriend.save(friendship2);
        repoFriend.save(friendship3);
        assert repoFriend.findOne(new Pair<Long>( list.get(1),list.get(0) )).equals(friendship1);
        assert repoFriend.findOne(new Pair<Long>( list.get(2),list.get(0) )).equals(friendship3);
        assert repoFriend.findOne(new Pair<Long>( list.get(2),list.get(1) )).equals(friendship2);

        User newUser = repoUsers.findOne(list.get(0));
        assert repoUsers.delete(newUser.getId()).equals(newUser);
        assert repoFriend.findOne(new Pair<Long>( list.get(1),list.get(0) )) == null;
        assert repoFriend.findOne(new Pair<Long>( list.get(2),list.get(0) )) == null;
        assert repoFriend.findOne(new Pair<Long>( list.get(2),list.get(1) )).equals(friendship2);
        clearDb();
    }

    public static void runUserDbTests(){
        runSaveTest();
        runUserFriendTest();
    }
}
