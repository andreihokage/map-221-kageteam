package ro.ubbcluj.map.ui;

import com.sun.security.jgss.GSSUtil;
import ro.ubbcluj.map.domain.Exceptions.*;
import ro.ubbcluj.map.domain.UserNetwork;
import ro.ubbcluj.map.domain.validators.ValidationException;
import ro.ubbcluj.map.service.FriendsipService;
import ro.ubbcluj.map.service.UserNetworkService;
import ro.ubbcluj.map.service.UserService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UI {

    private UserService userService;
    private FriendsipService friendsipService;
    private UserNetworkService userNetworkService;
    private Map< Integer , String > menuDescription;

    public UI(UserService userService ,FriendsipService friendsipService ,UserNetworkService userNetworkService) {
        this.userService = userService;
        this.friendsipService = friendsipService;
        this.userNetworkService = userNetworkService;
        menuDescription = new HashMap<>();
    }

    /**
     * add a user
     */
    private void addUser(){
        Scanner myObj = new Scanner(System.in);
        try {
            System.out.print("Enter firstname: ");
            String firstName = myObj.nextLine();

            System.out.print("Enter lastname: ");
            String lastName = myObj.nextLine();

            userService.addUser(firstName, lastName);
            System.out.println("The user was added!");
        } catch(ErrorAddUser e){
            System.out.println(e.getMessage());
        } catch(ValidationException e){
            System.out.println(e.getMessage());
        } catch(IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * remove a user
     */
    private void removeUser(){
        Scanner myObj = new Scanner(System.in);
        userService.findAllUsers().forEach(x-> System.out.println(x));
        try{
            System.out.print("Enter user id for remove: ");
            Long id = Long.parseLong(myObj.nextLine());
            userService.removeUser(id);
            System.out.println("The user was removed!");
        }catch(ErrorDeleteUser e){
            System.out.println(e.getMessage());
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * add a friendship
     */
    private  void addFriendship(){
        Scanner myObj = new Scanner(System.in);
        userService.findAllUsers().forEach(x-> System.out.println(x));
        try{
            System.out.print("Enter id of the first user:");
            Long id = Long.parseLong( myObj.nextLine() );
            System.out.print("Enter id of the second user: ");
            Long idFriend = Long.parseLong( myObj.nextLine() );
            friendsipService.addFriendship(id,idFriend);
            System.out.println("The friendship was created!");
        }catch (ErrorFindUser e){
            System.out.println(e.getMessage());
        }catch (ErrorAddFriendship e){
            System.out.println(e.getMessage());
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }catch (ValidationException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * remove a friendship
     */
    private void removeFriendship(){
        Scanner myObj = new Scanner(System.in);
        userService.findAllUsers().forEach(x-> System.out.println(x));
        try{
            System.out.print("Enter id of the first user:");
            Long id = Long.parseLong( myObj.nextLine() );
            System.out.print("Enter id of the second user: ");
            Long idFriend = Long.parseLong( myObj.nextLine() );
            friendsipService.removeFriendship(id,idFriend);
            System.out.println("The friendship was removed!");
        }catch (ErrorFindUser e){
            System.out.println(e.getMessage());
        }catch (ErrorRemoveFriendship e){
            System.out.println(e.getMessage());
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * number of communities
     */
    private void numberOfCommunities(){
        int ans = userNetworkService.countNumberCommunities();
        System.out.println("Number of communities is: " + ans );
    }

    /**
     * the most sociable community
     */
    private void displayMostSociable(){
        UserNetwork userNetwork = userNetworkService.computeLongestPath();
        System.out.println(userNetwork);
    }

    /**
     * display all users
     */
    private void displayAllUsers(){
        userService.findAllUsers().forEach( x -> System.out.println(x) );
    }

    /**
     * display all friendships
     */
    private void displayAllFriendships(){
        friendsipService.findAllFriendships().forEach( x -> System.out.println(x) );
    }

    /**
     * create a menu
     */
    private void createMenu(){
        menuDescription.put(0,"Exit app!");
        menuDescription.put(1,"Add User!");
        menuDescription.put(2,"Remove User!");
        menuDescription.put(3,"Add friendship!");
        menuDescription.put(4,"Remove friendship!");
        menuDescription.put(5,"Number of Communities!");
        menuDescription.put(6,"Display most sociable community!");
        menuDescription.put(7,"Display all users!");
        menuDescription.put(8,"Display all friendships!");
    }

    private void printMenu(){
        // menuDescription.entrySet().forEach(x -> System.out.println(x));
        System.out.println("-------------------------------------MENU-------------------------------------");
        for (Map.Entry<Integer, String> x : menuDescription.entrySet())
            System.out.println( x.getKey() + "->" + x.getValue() );
        System.out.println("------------------------------------------------------------------------------");
    }

    public void run(){
        boolean stop = false;
        createMenu();
        Scanner myObj = new Scanner(System.in);
        while ( true ){
            printMenu();
            System.out.print("Choose Option: ");
            Integer option = -1;
            try {
                option = Integer.parseInt(myObj.nextLine());
            }catch (NumberFormatException e){

            }

            switch (option){
                case 0:
                    stop = true;
                    break;
                case 1:
                    addUser();
                    break;
                case 2:
                    removeUser();
                    break;
                case 3:
                    addFriendship();
                    break;
                case 4:
                    removeFriendship();
                    break;
                case 5:
                    numberOfCommunities();
                    break;
                case 6:
                    displayMostSociable();
                    break;
                case 7:
                    displayAllUsers();
                    break;
                case 8:
                     displayAllFriendships();
                     break;
                default:
                    System.out.println("Invalid option!");
            }

            if( stop == true )
                break;

            System.out.println("\nPress Enter to continue!");
            try {
                System.in.read();
                System.in.skipNBytes(System.in.available());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
